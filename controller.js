var serverName = process.argv[2] || 'http://0.0.0.0:3000';
var droneControllerPath = '/home/matt/projects/perso/parrotDrone/Samples/Unix/JumpingSumoAll'
var spawn = require('child_process').spawn;

var drone = spawn('make', ['run'], {cwd : droneControllerPath})
drone.on('close', function (code) {})

var running = false;

drone.stdout.on('data', function (data) {
    if (!running && data.toString('utf8').indexOf('Running') > -1) {
        console.log('Drone under control')
        running = true;

        spawn('avconv' ,
            //['-f', 'video4linux2', '-r', '25', '-i', '/dev/video0', '-f', 'mpeg1video', '-r', '24', '-flags', 'mv0',
            ['-f', 'image2', '-f', 'mjpeg', '-i', 'video_fifo', '-f', 'mpeg1video', '-r', '24',
            serverName + '/receiver'],
            {cwd : droneControllerPath}
        ).stderr.on('data', function(data) {console.log('video : ' + data)})

        var io = require('socket.io-client')
        var socket = io.connect(serverName + '/controller');
        socket.on('connect', function () {
          socket.on('code', function(code){
                 if (38 == code) drone.stdin.write('k')
            else if (40 == code) drone.stdin.write('j')
            else if (37 == code) drone.stdin.write('h')
            else if (39 == code) drone.stdin.write('l')
            else if (32 == code) drone.stdin.write(' ')
                else return;
                console.log('input key code : ' + code)
          })
        });
    }
});