var VIDEO_SIZE_WIDTH  = 640,
    VIDEO_SIZE_HEIGHT = 480;

// bootstrap
var express = require('express')
var app = express()
app.use(require('body-parser').urlencoded({extended: true}))
app.use(require('cookie-parser')())

// Templating
app.set('view engine', 'html')
app.set('layout', 'layout')
app.enable('view cache')
app.engine('html', require('hogan-express'))
app.use('/static', express.static(__dirname + '/assets'))

//Controller
app.get('/', function (req, res) {
  res.render('index')
})
app.post('/receiver', function (req, res) {
    req.on('data', vwss.broadcast);
})

// http server
var server = require("http").createServer(app)
server.listen(process.env.PORT || 3000, function () {
  console.log('App listening at http://%s:%s', server.address().address, server.address().port)
})

//Command socket
var io = require('socket.io')(server),
ioCommand    = io.of('/command'),
ioController = io.of('/controller');
ioCommand.on('connection', function (socket) {
  socket.on('code', function(code) {
    ioController.emit('code', code)
  })
});

//Video Socket server
var WebSocketServer = require('ws').Server
  , vwss = new WebSocketServer({server: server});

vwss.broadcast = function broadcast(data) {
  vwss.clients.forEach(function each(client) {
    client.send(data);
  });
};

vwss.on('connection', function(socket) {
    var streamHeader = new Buffer(8);
    streamHeader.write('jsmp');
    streamHeader.writeUInt16BE(VIDEO_SIZE_WIDTH, 4);
    streamHeader.writeUInt16BE(VIDEO_SIZE_HEIGHT, 6);
    socket.send(streamHeader, {binary:true});
});

console.log("websocket server created")